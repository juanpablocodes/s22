/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/
    let registeredUsers = [

        "Kobe Bryant",
        "Michael Jordan",
        "Lebron James",
        "Kevin Durant",
        "Stephen Curry",
        "Allen Iverson",
        "Vince Carter"
    ];

    let friendsList = [];



/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/

function addFriend(userName) {
    let ifExist = registeredUsers.includes(userName);

    if(ifExist) {
        alert("Registration failed. Username already exists!")
        console.log("Register: " + userName)
    } else {
        registeredUsers.push(userName);
        alert("Thank you for registering!");
        console.log("Register: " + userName);
    }
};

addFriend("Derek Rose");
console.log("Registered Users: ");
console.log(registeredUsers);

addFriend("Ja Morant");
console.log("Registered Users: ");
console.log(registeredUsers);

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.
*/

function AddRegisteredFriend (newFriend) {
    let regFriend = registeredUsers.includes(newFriend);

    if(regFriend) {
        friendsList.push(newFriend)
        alert("You have added " + newFriend + " as a friend!");
        console.log("Added a friend: ");
    console.log(newFriend);
    } else {
        alert("User not found!")
        console.log("Added a friend: ");
        console.log(newFriend);
    }
}

    AddRegisteredFriend("Kobe bryant");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")

    AddRegisteredFriend("Lebron James");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")

    AddRegisteredFriend("Kevin Durant");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")

    AddRegisteredFriend("Allen Iverson");
    console.log("Friends List:  ")
    console.log(friendsList);

    console.log("")



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
*/

console.log("My Friends: ");

friendsList.forEach(function(entry) {
    console.log(entry);
})

console.log(" ")
    
// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

function myFriends (squad) {
    let beMyFriend = friendsList.length;

    if(beMyFriend ++) {
        alert("You currently have " + friendsList.length + " friends")
    } else {
        alert("You currently have " + friendsList.length + " friends. Add one first.")
    }
}

myFriends(friendsList.length)
console.log("My Friends: " + friendsList.length);


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/




/*======================================================================================*/



// Try this for fun:
/*
   

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/





